# Retruco Quiz (DEPRECATED)


_Web widget that always questions_

This repository is now **deprecated**. Development continues on [retruco](https://framagit.org/retruco/retruco) repository.

The main repository for this project is hosted on [FramaGit](https://framagit.org/): https://framagit.org/retruco/quiz

## Install

```bash
git clone https://framagit.org/retruco/quiz.git
cd quiz/
```

Create a `.env` file to set PostgreSQL database informations (you can use `example.env` as a template). Then

```bash
npm install
```

### Database Creation

#### When using _Debian GNU/Linux_

As `root` user:
```bash
apt install postgresql postgresql-contrib

su - postgres
createuser -D -P -R -S quiz
  Enter password for new role: quiz
  Enter it again: quiz
createdb -E utf-8 -O quiz quiz

psql quiz
CREATE EXTENSION IF NOT EXISTS pg_trgm;
exit
```

### Database Configuration

```bash
npm run configure
psql -h 127.0.0.1 -U quiz -W quiz
  Password: quiz
INSERT INTO clients (segment, name, token) VALUES ('demo', 'Démonstration', 'demo');
exit
```

## Server Launch

In development mode:

```bash
npm run dev
```

In production mode:

```bash
npm run build
npm start
```
