import { writable } from "svelte/store"

import {generateBundles, Localization} from "./locales"

export const language = writable(null)
export const localize = writable((id, args, fallback) => fallback || id)

language.subscribe((newLanguage) => {
  const localization = new Localization(generateBundles([newLanguage]))
  localize.set(localization.getString.bind(localization))
})
