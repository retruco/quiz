import AutocompleteOrCreate from "./AutocompleteOrCreate.svelte"
import InputDate from "./InputDate.svelte"
import InputEmail from "./InputEmail.svelte"
import InputText from "./InputText.svelte"
import Knowledge from "./Knowledge.svelte"
import RadioButtons from "./RadioButtons.svelte"
import YesNoButtons from "./YesNoButtons.svelte"

export default {
  AutocompleteOrCreate,
  InputDate,
  InputEmail,
  InputText,
  Knowledge,
  RadioButtons,
  YesNoButtons,
}
