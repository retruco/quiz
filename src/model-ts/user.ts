export class User {
  constructor(
    public id: number,
    public clientSegment: string,
    public username: string,
  ) {}
  static fromEntry(entry: UserEntry | null): User | null {
    return entry === null
      ? null
      : new User(entry.id, entry.client_segment, entry.username)
  }
}

export interface UserEntry {
  client_segment: string
  id: number
  username: string
}
