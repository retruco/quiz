import { Answer } from "./answer"
import { db } from "./database"

export async function getAnswer(
  path: string,
  userId: number,
): Promise<Answer | null> {
  return Answer.fromEntry(
    await db.oneOrNone(
      `
          SELECT *
          FROM answers
          WHERE
            question_path = $<path>
            AND user_id = $<userId>
        `,
      {
        path,
        userId,
      },
    ),
  )
}
