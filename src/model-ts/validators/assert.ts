import assert from "assert"

export function assertValid([data, error]: [any, any]): any {
  assert(
    error === null,
    `Error ${JSON.stringify(error, null, 2)} for ${JSON.stringify(
      data,
      null,
      2,
    )}`,
  )
  return data
}
