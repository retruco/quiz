export class Session {
  constructor(
    public token: string,
    public clientSegment: string,
    public expires: Date,
    public language: string,
    public userId: number,
  ) {}
  static fromEntry(entry: SessionEntry | null): Session | null {
    return entry === null
      ? null
      : new Session(
          entry.token,
          entry.client_segment,
          entry.expires,
          entry.language,
          Number(entry.user_id),
        )
  }
}

export interface SessionEntry {
  token: string
  client_segment: string
  expires: Date
  language: string
  user_id: string
}
