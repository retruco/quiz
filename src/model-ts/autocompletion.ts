import {
  validateInteger,
  validateNonEmptyTrimmedString,
  validateNumber,
} from "./validators/core"

export class Autocompletion {
  constructor(
    public domain: string,
    public id: number,
    public slug: string,
    public text: string,
  ) {}

  static fromEntry(entry: AutocompletionEntry | null): Autocompletion | null {
    return entry === null
      ? null
      : new Autocompletion(
          entry.domain,
          Number(entry.id),
          entry.slug,
          entry.text,
        )
  }

  static fromValidJson(data: AutocompletionJson): Autocompletion {
    return new this(data.domain, data.id, data.slug, data.text)
  }

  toJson(): AutocompletionJson {
    return {
      domain: this.domain,
      id: this.id,
      slug: this.slug,
      text: this.text,
    }
  }

  static validateJson(data: any): [any, any] {
    if (data === null || data === undefined) {
      return [data, "Missing value"]
    }
    if (typeof data !== "object") {
      return [data, `Expected an object got "${typeof data}"`]
    }

    data = { ...data }
    const errors: { [key: string]: any } = {}
    const remainingKeys = new Set(Object.keys(data))

    for (const key of ["domain", "slug", "text"]) {
      remainingKeys.delete(key)
      const [value, error] = validateNonEmptyTrimmedString(data[key])
      data[key] = value
      if (error !== null) {
        errors[key] = error
      }
    }

    {
      const key = "id"
      remainingKeys.delete(key)
      const [value, error] = validateInteger(data[key])
      data[key] = value
      if (error !== null) {
        errors[key] = error
      }
    }

    for (let key of remainingKeys) {
      errors[key] = "Unexpected entry"
    }
    return Object.keys(errors).length === 0
      ? [Autocompletion.fromValidJson(data), null]
      : [data, errors]
  }
}

export interface AutocompletionEntry {
  domain: string
  id: string
  slug: string
  text: string
}

export interface AutocompletionJson {
  domain: string
  id: number
  slug: string
  text: string
}

export class AutocompletionWithDistance extends Autocompletion {
  constructor(
    public distance: number,
    domain: string,
    id: number,
    slug: string,
    text: string,
  ) {
    super(domain, id, slug, text)
  }

  static fromEntry(entry: AutocompletionWithDistanceEntry | null): AutocompletionWithDistance | null {
    return entry === null
      ? null
      : new AutocompletionWithDistance(
          entry.distance,
          entry.domain,
          Number(entry.id),
          entry.slug,
          entry.text,
        )
  }

  static fromValidJson(data: AutocompletionWithDistanceJson): AutocompletionWithDistance {
    return new this(data.distance, data.domain, data.id, data.slug, data.text)
  }

  toJson(): AutocompletionWithDistanceJson {
    return {
      ...super.toJson(),
      distance: this.distance,
    }
  }

  static validateJson(data: any): [any, any] {
    if (data === null || data === undefined) {
      return [data, "Missing value"]
    }
    if (typeof data !== "object") {
      return [data, `Expected an object got "${typeof data}"`]
    }

    data = { ...data }
    const errors: { [key: string]: any } = {}
    const remainingKeys = new Set(Object.keys(data))

    {
      const key = "distance"
      remainingKeys.delete(key)
      const [value, error] = validateNumber(data[key])
      data[key] = value
      if (error !== null) {
        errors[key] = error
      }
    }

    for (const key of ["domain", "slug", "text"]) {
      remainingKeys.delete(key)
      const [value, error] = validateNonEmptyTrimmedString(data[key])
      data[key] = value
      if (error !== null) {
        errors[key] = error
      }
    }

    {
      const key = "id"
      remainingKeys.delete(key)
      const [value, error] = validateInteger(data[key])
      data[key] = value
      if (error !== null) {
        errors[key] = error
      }
    }

    for (let key of remainingKeys) {
      errors[key] = "Unexpected entry"
    }
    return Object.keys(errors).length === 0
      ? [Autocompletion.fromValidJson(data), null]
      : [data, errors]
  }
}

export interface AutocompletionWithDistanceEntry extends AutocompletionEntry {
  distance: number
}

export interface AutocompletionWithDistanceJson extends AutocompletionJson {
  distance: number
}
