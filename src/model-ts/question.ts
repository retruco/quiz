import { Answer } from "./answer"
import {
  validateBoolean,
  validateInteger,
  validateMissing,
  validateNonEmptyTrimmedString,
  validateOption,
} from "./validators/core"

export class Question {
  static classByName: { [className: string]: typeof Question } = {}

  constructor(
    public path: string,
    public label: string,
    public isLabelHtml: boolean,
    public options: any = null,
  ) {}

  static fromValidJson(data: QuestionJson): Question {
    return Question.classByName[data.className].fromValidJsonContent(data)
  }

  static fromValidJsonContent(data: QuestionJson): Question {
    return new this(data.path, data.label, data.isLabelHtml, data.options)
  }

  static register(subClass: any) {
    Question.classByName[subClass.name] = subClass
  }

  static validateJson(data: any): [any, any] {
    if (data === null || data === undefined) {
      return [data, "Missing value"]
    }
    if (typeof data !== "object") {
      return [data, `Expected an object got "${typeof data}"`]
    }

    data = { ...data }
    const errors: { [key: string]: any } = {}
    const remainingKeys = new Set(Object.keys(data))

    remainingKeys.delete("className")
    const [className, classNameError] = validateNonEmptyTrimmedString(
      data.className,
    )
    data.className = className
    let questionClass: any
    if (classNameError === null) {
      questionClass = Question.classByName[className]
      if (questionClass === undefined) {
        errors.className = "Unkwnown question class name"
      } else {
        data = questionClass.validateJsonContent(data, errors, remainingKeys)
      }
    } else {
      errors.className = classNameError
    }

    for (let key of remainingKeys) {
      errors[key] = "Unexpected entry"
    }
    return Object.keys(errors).length === 0
      ? [questionClass!.fromValidJsonContent(data), null]
      : [data, errors]
  }

  static validateJsonContent(
    data: any,
    errors: { [key: string]: any },
    remainingKeys: Set<string>,
  ) {
    {
      const key = "isLabelHtml"
      remainingKeys.delete(key)
      const [value, error] = validateBoolean(data[key])
      data[key] = value
      if (error !== null) {
        errors[key] = error
      }
    }

    // TODO: Check that `path` is a valid JSON path.
    for (let key of ["label", "path"]) {
      remainingKeys.delete(key)
      const [value, error] = validateNonEmptyTrimmedString(data[key])
      data[key] = value
      if (error !== null) {
        errors[key] = error
      }
    }

    // TODO: Use static validateQuestionOptions()
    {
      const key = "options"
      remainingKeys.delete(key)
      const [value, error] = [data[key], null]
      data[key] = value
      if (error !== null) {
        errors[key] = error
      }
    }

    return data
  }

  toJson(): QuestionJson {
    return {
      className: this.constructor.name,
      isLabelHtml: this.isLabelHtml,
      label: this.label,
      options: this.options,
      path: this.path,
    }
  }

  validateAnswerJson(data: any): [any, any] {
    if (data === null || data === undefined) {
      return [data, "Missing value"]
    }
    if (typeof data !== "object") {
      return [data, `Expected an object got "${typeof data}"`]
    }

    data = { ...data }
    const errors: { [key: string]: any } = {}
    const remainingKeys = new Set(Object.keys(data))

    for (let key of ["external"]) {
      remainingKeys.delete(key)
      const [value, error] = validateBoolean(data[key])
      data[key] = value
      if (error !== null) {
        errors[key] = error
      }
    }

    for (let key of ["questionPath", "updated"]) {
      remainingKeys.delete(key)
      const [value, error] = validateNonEmptyTrimmedString(data[key])
      data[key] = value
      if (error !== null) {
        errors[key] = error
      }
    }

    for (let key of ["userId"]) {
      remainingKeys.delete(key)
      const [value, error] = validateInteger(data[key])
      data[key] = value
      if (error !== null) {
        errors[key] = error
      }
    }

    for (let key of ["value"]) {
      remainingKeys.delete(key)
      const [value, error] = validateOption(
        validateMissing,
        this.validateAnswerValueJson.bind(this),
      )(data[key])
      data[key] = value
      if (error !== null) {
        errors[key] = error
      }
    }

    for (let key of remainingKeys) {
      errors[key] = "Unexpected entry"
    }
    return Object.keys(errors).length === 0
      ? [
          new Answer(
            data.external,
            data.questionPath,
            new Date(data.updated),
            data.userId,
            data.value,
          ),
          null,
        ]
      : [data, errors]
  }

  validateAnswerValueJson(data: any): [any, any] {
    return [
      data,
      `Class ${
        this.constructor.name
      } doesn't know how to validate the JSON of an answer value`,
    ]
  }
}

export interface QuestionJson {
  className: string
  isLabelHtml: boolean
  label: string
  options: any
  path: string
}

export class BooleanQuestion extends Question {
  static widgetSymbol = "YesNoButtons"

  validateAnswerValueJson(data: any): [any, any] {
    return validateBoolean(data)
  }
}
Question.register(BooleanQuestion)

export class DateQuestion extends Question {
  static widgetSymbol = "InputDate"

  validateAnswerValueJson(data: any): [any, any] {
    return validateNonEmptyTrimmedString(data)
  }
}
Question.register(DateQuestion)

export class EmailQuestion extends Question {
  static widgetSymbol = "InputEmail"

  validateAnswerValueJson(data: any): [any, any] {
    return validateNonEmptyTrimmedString(data)
  }
}
Question.register(EmailQuestion)

export class TextQuestion extends Question {
  static widgetSymbol = "InputText"

  validateAnswerValueJson(data: any): [any, any] {
    return validateNonEmptyTrimmedString(data)
  }
}
Question.register(TextQuestion)
