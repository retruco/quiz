import { Autocompletion, AutocompletionJson } from "../autocompletion"
import { Question, QuestionJson } from "../question"
import { validateBoolean } from "../validators/core"

export class Knowledge extends Question {
  static widgetSymbol = "Knowledge"

  constructor(
    path: string,
    label: string,
    isLabelHtml: boolean,
    public autocompletion: Autocompletion,
    options: any = null,
  ) {
    super(path, label, isLabelHtml, options)
  }

  static fromValidJsonContent(data: KnowledgeJson): Knowledge {
    return new this(
      data.path,
      data.label,
      data.isLabelHtml,
      Autocompletion.fromValidJson(data.autocompletion),
      data.options,
    )
  }

  static validateJsonContent(
    data: any,
    errors: { [key: string]: any },
    remainingKeys: Set<string>,
  ) {
    data = super.validateJsonContent(data, errors, remainingKeys)

    {
      const key = "autocompletion"
      remainingKeys.delete(key)
      const [value, error] = Autocompletion.validateJson(data[key])
      data[key] = value
      if (error !== null) {
        errors[key] = error
      }
    }

    return data
  }

  toJson(): KnowledgeJson {
    return {
      autocompletion: this.autocompletion.toJson(),
      className: this.constructor.name,
      isLabelHtml: this.isLabelHtml,
      label: this.label,
      options: this.options,
      path: this.path,
    }
  }

  validateAnswerValueJson(data: any): [any, any] {
    return validateBoolean(data)
  }
}
Question.register(Knowledge)

interface KnowledgeJson extends QuestionJson {
  autocompletion: AutocompletionJson
}
