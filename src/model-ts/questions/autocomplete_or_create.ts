import { Question, QuestionJson } from "../question"
import { validateNonEmptyTrimmedString, validateNumber } from "../validators/core"

export class AutocompleteOrCreate extends Question {
  static widgetSymbol = "AutocompleteOrCreate"

  constructor(
    path: string,
    label: string,
    isLabelHtml: boolean,
    public domain: string,
    options: any = null,
  ) {
    super(path, label, isLabelHtml, options)
  }

  static fromValidJsonContent(data: AutocompleteOrCreateJson): AutocompleteOrCreate {
    return new this(data.path, data.label, data.isLabelHtml, data.domain, data.options)
  }

  static validateJsonContent(data: any, errors: {[key:string]: any}, remainingKeys: Set<string>) {
    data = super.validateJsonContent(data, errors, remainingKeys)

    {
      const key = "domain"
      remainingKeys.delete(key)
      const [value, error] = validateNonEmptyTrimmedString(data[key])
      data[key] = value
      if (error !== null) {
        errors[key] = error
      }
    }

    return data
  }

  toJson(): AutocompleteOrCreateJson {
    return {
      className: this.constructor.name,
      domain: this.domain,
      isLabelHtml: this.isLabelHtml,
      label: this.label,
      options: this.options,
      path: this.path,
    }
  }

  validateAnswerValueJson(value: any): [any, any] {
    return validateNumber(value)
  }
}
Question.register(AutocompleteOrCreate)

interface AutocompleteOrCreateJson extends QuestionJson {
  domain: string
}