import { Question, QuestionJson } from "../question"
import {
  validateArray,
  validateChoice,
  validateNonEmptyTrimmedString,
} from "../validators/core"

export interface ChoiceItem {
  label: string
  value: string
}

export class SingleChoice extends Question {
  static widgetSymbol = "RadioButtons"

  choices: ChoiceItem[]

  constructor(
    path: string,
    label: string,
    isLabelHtml: boolean,
    choices: (ChoiceItem | string)[],
    options: any = null,
  ) {
    super(path, label, isLabelHtml, options)
    this.choices = choices.map(choice =>
      typeof choice === "string" ? { label: choice, value: choice } : choice,
    )
  }

  static fromValidJsonContent(data: SingleChoiceJson): SingleChoice {
    return new this(
      data.path,
      data.label,
      data.isLabelHtml,
      data.choices,
      data.options,
    )
  }

  static validateJsonContent(
    data: any,
    errors: { [key: string]: any },
    remainingKeys: Set<string>,
  ) {
    data = super.validateJsonContent(data, errors, remainingKeys)

    {
      const key = "choices"
      remainingKeys.delete(key)
      const [value, error] = validateArray(validateChoiceItem)(data[key])
      data[key] = value
      if (error !== null) {
        errors[key] = error
      }
    }

    return data
  }

  toJson(): SingleChoiceJson {
    return {
      choices: this.choices,
      className: this.constructor.name,
      isLabelHtml: this.isLabelHtml,
      label: this.label,
      options: this.options,
      path: this.path,
    }
  }

  validateAnswerValueJson(value: any): [any, any] {
    return validateChoice(this.choices.map(choice => choice.value))(value)
  }
}
Question.register(SingleChoice)

interface SingleChoiceJson extends QuestionJson {
  choices: ChoiceItem[]
}

function validateChoiceItem(data: any): [any, any] {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object got "${typeof data}"`]
  }

  data = { ...data }
  const errors: { [key: string]: any } = {}
  const remainingKeys = new Set(Object.keys(data))

  for (let key of ["label", "value"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}
