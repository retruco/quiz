import { Answer } from "./answer"
import { getAnswer } from "./orm"
import { Question } from "./question"
import { Session } from "./session"

export class Step {
  question?: Question
  state?: any

  constructor(
    question: Question | null | undefined,
    state: any,
    public answered: boolean = false,
    public answer?: Answer,
  ) {
    if (question !== undefined && question !== null) {
      this.question = question
    }
    if (state !== undefined && state !== null) {
      this.state = state
    }
  }
}

/// Default function that retrieves answer for step and considers question to be answered
/// when answer exits and is not dated from more than 3 months.
export async function answerStep(session: Session, step: Step): Promise<void> {
  if (step.question !== undefined) {
    const answer = await getAnswer(step.question.path, session.userId)
    if (answer !== null) {
      step.answer = answer
      const now = new Date()
      const threeMonthsAgo = new Date(
        now.getFullYear(),
        now.getMonth() - 3,
        now.getDate(),
      )
      if (answer.updated >= threeMonthsAgo) {
        step.answered = true
      }
    }
  }
}