require("dotenv").config()

import { validateServerConfig } from "./validators/server_config"

const serverConfig = {
  db: {
    database: process.env.RETRUCO_QUIZ_DB_NAME || "quiz",
    host: process.env.RETRUCO_QUIZ_DB_HOST || "localhost",
    password: process.env.RETRUCO_QUIZ_DB_PASSWORD || "quiz", // Change it!
    port: process.env.RETRUCO_QUIZ_DB_PORT || 5432,
    user: process.env.RETRUCO_QUIZ_DB_USER || "quiz",
  },
  sessionSecret: "retruco-quiz secret", // Change it!
}

const [validServerConfig, error] = validateServerConfig(serverConfig)
if (error !== null) {
  console.error(
    `Error in server configuration:\n${JSON.stringify(
      validServerConfig,
      null,
      2,
    )}\nError:\n${JSON.stringify(error, null, 2)}`,
  )
  process.exit(-1)
}

export default validServerConfig
