import { Session } from "./session"
import { Step } from "./step"
import { validateNonEmptyTrimmedString } from "./validators/core"

export class Survey {
  static bySegment: { [segment: string]: Survey } = {}

  constructor(
    public segment: string,
    public name: string,
    // Method `walkQuiz` is only present in server objects.
    public walkQuiz?: (
      rootSurvey: Survey,
      session: Session,
      state?: any,
    ) => Promise<Step>,
    // Method `walkList` is only present in server objects.
    public walkList?: (
      rootSurvey: Survey,
      session: Session,
      state?: any,
    ) => Promise<Step>,
    // Method `replaceQuestionState` may only be present in server objects.
    public replaceQuestionState?: (
      session: Session,
      currentState: any,
      questionState: any,
    ) => Promise<[Step, any]>,
  ) {}

  static register(instance: Survey) {
    Survey.bySegment[instance.segment] = instance
    return instance
  }

  static validateJson(data: any): [any, any] {
    if (data === null || data === undefined) {
      return [data, "Missing value"]
    }
    if (typeof data !== "object") {
      return [data, `Expected an object got "${typeof data}"`]
    }

    data = { ...data }
    const errors: { [key: string]: any } = {}
    const remainingKeys = new Set(Object.keys(data))

    for (let key of ["name", "segment"]) {
      remainingKeys.delete(key)
      const [value, error] = validateNonEmptyTrimmedString(data[key])
      data[key] = value
      if (error !== null) {
        errors[key] = error
      }
    }

    for (let key of remainingKeys) {
      errors[key] = "Unexpected entry"
    }
    return Object.keys(errors).length === 0
      ? [new Survey(data.segment, data.name), null]
      : [data, errors]
  }

  toJson(): SurveyJson {
    return {
      name: this.name,
      segment: this.segment,
    }
  }
}

export interface SurveyJson {
  name: string
  segment: string
}
