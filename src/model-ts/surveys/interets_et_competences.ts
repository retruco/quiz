import assert from "assert"

import { Autocompletion } from "../autocompletion"
import { db } from "../database"
import { getAnswer } from "../orm"
import { Knowledge } from "../questions/knowledge"
import { Session } from "../session"
import { Step, answerStep } from "../step"
import { Survey } from "../survey"
import {
  validateChoice,
  validateInteger,
  validateMissing,
  validateNumber,
  validateOption,
  validateSetValue,
} from "../validators/core"

interface State {
  autocompletionId: number
  index: StateIndex
}

enum StateIndex {
  INTEREST, // interested by
  WORK, // worked on it
}

const domains = ["interets", "travaux"]

async function newStep(
  autocompletion: Autocompletion | null,
  index: StateIndex,
  previousState: State | null | undefined,
  session: Session,
) {
  if (autocompletion === null) {
    // Survey is finished.
    return new Step(null, previousState)
  }
  const step = new Step(
    new Knowledge(
      `${domains[index]}[${autocompletion.id}]`,
      index === StateIndex.WORK
        ? "Avez-vous déjà travaillé sur :"
        : "Êtes-vous intéressé par :",
      false,
      autocompletion,
    ),
    { autocompletionId: autocompletion.id, index },
  )
  await answerStep(session, step)
  return step
}

async function replaceQuestionState(
  session: Session,
  _currentState: State,
  questionState: any,
): Promise<[Step, any]> {
  const [validatedQuestionState, questionStateError] = validateState(questionState)
  if (questionStateError !== null) {
    return [new Step(null, validatedQuestionState), questionStateError]
  }
  if (validatedQuestionState.index !== StateIndex.INTEREST) {
    return [
      new Step(null, validatedQuestionState),
      { index: `Value must be "StateIndex.INTEREST" (${StateIndex.INTEREST})` },
    ]
  }

  const autocompletion = Autocompletion.fromEntry(
    await db.oneOrNone(
      `
        SELECT *
        FROM autocompletions
        WHERE
          domain = $<domain>
          AND id = $<id>
        LIMIT 1
      `,
      {
        domain: domains[StateIndex.INTEREST],
        id: validatedQuestionState.autocompletionId,
      },
    ),
  )
  return [
    await newStep(autocompletion, StateIndex.INTEREST, validatedQuestionState, session),
    autocompletion === null ? { autocompletionId: "Unknown ID" } : null,
  ]
}

function validateState(data: any): [any, any] {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object got "${typeof data}"`]
  }

  data = { ...data }
  const errors: { [key: string]: any } = {}
  const remainingKeys = new Set(Object.keys(data))

  for (let key of ["autocompletionId"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNumber(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of ["index"]) {
    remainingKeys.delete(key)
    const [value, error] = validateOption(
      [validateMissing, validateSetValue(StateIndex.INTEREST)],
      [validateInteger, validateChoice([StateIndex.INTEREST, StateIndex.WORK])],
    )(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

/// Walk a few steps to next question not yet asked (and its answer).
// Include answer when it exists.
async function walkQuiz(
  rootSurvey: Survey,
  session: Session,
  state?: State,
): Promise<Step> {
  const autocompletionId: number | null =
    state === undefined ? null : state.autocompletionId
  const index: StateIndex = state === undefined ? StateIndex.WORK : state.index

  if (index === StateIndex.INTEREST) {
    // if user was interested by topic, ask him if he has ever worked on this topic.
    assert.notStrictEqual(autocompletionId, null)
    const interestAnswer = await getAnswer(
      `${domains[StateIndex.INTEREST]}[${autocompletionId}]`,
      session.userId,
    )
    if (interestAnswer !== null && interestAnswer.value) {
      const autocompletion = Autocompletion.fromEntry(
        await db.oneOrNone(
          `
            SELECT *
            FROM autocompletions
            WHERE
              domain = $<domain>
              AND id = $<autocompletionId>
          `,
          {
            autocompletionId,
            domain: domains[StateIndex.INTEREST],
          },
        ),
      )
      assert.notStrictEqual(autocompletion, null)
      return await newStep(autocompletion, StateIndex.WORK, state, session)
    }
  }
  const autocompletion = Autocompletion.fromEntry(
    await db.oneOrNone(
      `
        SELECT *
        FROM autocompletions
        WHERE
          domain = $<domain>
          AND domain || '[' || id || ']' NOT IN (
            SELECT question_json->>'path'
            FROM asked_questions
            WHERE
              survey_segment = $<surveySegment>
              AND user_id = $<userId>
              AND question_json->>'path' LIKE $<domain> || '[%]'
          )
        ORDER BY id
        LIMIT 1
      `,
      {
        domain: domains[StateIndex.INTEREST],
        surveySegment: rootSurvey.segment,
        userId: session.userId,
      },
    ),
  )
  return await newStep(autocompletion, StateIndex.INTEREST, state, session)
}

/// Walk a single state to next question (and its answer).
async function walkList(
  rootSurvey: Survey,
  session: Session,
  state?: State,
): Promise<Step> {
  const autocompletionId: number | null =
    state === undefined ? null : state.autocompletionId
  const index: StateIndex = state === undefined ? StateIndex.WORK : state.index

  if (index === StateIndex.INTEREST) {
    // if user was interested by topic, ask him if he has ever worked on this topic.
    assert.notStrictEqual(autocompletionId, null)
    const interestAnswer = await getAnswer(
      `${domains[StateIndex.INTEREST]}[${autocompletionId}]`,
      session.userId,
    )
    if (interestAnswer !== null && interestAnswer.value) {
      const autocompletion = Autocompletion.fromEntry(
        await db.oneOrNone(
          `
            SELECT *
            FROM autocompletions
            WHERE
              domain = $<domain>
              AND id = $<autocompletionId>
          `,
          {
            autocompletionId,
            domain: domains[StateIndex.INTEREST],
          },
        ),
      )
      assert.notStrictEqual(autocompletion, null)
      return await newStep(autocompletion, StateIndex.WORK, state, session)
    }
  }
  const whereClauses = ["domain = $<domain>"]
  if (autocompletionId !== null) {
    whereClauses.push("id > $<autocompletionId>")
  }
  const autocompletion = Autocompletion.fromEntry(
    await db.oneOrNone(
      `
        SELECT *
        FROM autocompletions
        WHERE
          ${whereClauses.join(" AND ")}
          AND id NOT IN (
            SELECT substring(question_json->>'path' from '\\d+')::int
            FROM asked_questions
            WHERE
              survey_segment = $<surveySegment>
              AND user_id = $<userId>
              AND question_json->>'path' LIKE $<domain> || '[%]'
          )
        ORDER BY id
        LIMIT 1
      `,
      {
        autocompletionId,
        domain: domains[StateIndex.INTEREST],
        surveySegment: rootSurvey.segment,
        userId: session.userId,
      },
    ),
  )

  return await newStep(autocompletion, StateIndex.INTEREST, state, session)
}

export default Survey.register(
  new Survey(
    "interets_et_competences",
    "Centres d'intérêts et compétences",
    walkQuiz,
    walkList,
    replaceQuestionState,
  ),
)
