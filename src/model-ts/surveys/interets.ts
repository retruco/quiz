import { Autocompletion } from "../autocompletion"
import { db } from "../database"
import { Knowledge } from "../questions/knowledge"
import { Session } from "../session"
import { Step, answerStep } from "../step"
import { Survey } from "../survey"
import { validateNumber } from "../validators/core"

interface State {
  autocompletionId: number
}

async function newStep(
  autocompletion: Autocompletion | null,
  domain: string,
  previousState: State | null | undefined,
  session: Session,
) {
  if (autocompletion === null) {
    // Survey is finished.
    return new Step(null, previousState)
  }

  const step = new Step(
    new Knowledge(
      `${domain}[${autocompletion.id}]`,
      "Êtes-vous intéressé par :",
      false,
      autocompletion,
    ),
    { autocompletionId: autocompletion.id },
  )
  await answerStep(session, step)
  return step
}

async function replaceQuestionState(
  session: Session,
  _currentState: State,
  questionState: any,
): Promise<[Step, any]> {
  const [validatedQuestionState, questionStateError] = validateState(
    questionState,
  )
  if (questionStateError !== null) {
    return [new Step(null, validatedQuestionState), questionStateError]
  }

  const domain = "interets"
  const autocompletion = Autocompletion.fromEntry(
    await db.oneOrNone(
      `
        SELECT *
        FROM autocompletions
        WHERE
          domain = $<domain>
          AND id = $<id>
        LIMIT 1
      `,
      {
        domain,
        id: validatedQuestionState.autocompletionId,
      },
    ),
  )
  return [
    await newStep(
      autocompletion,
      domain,
      validatedQuestionState,
      session,
    ),
    autocompletion === null ? { autocompletionId: "Unknown ID" } : null,
  ]
}

function validateState(data: any): [any, any] {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object got "${typeof data}"`]
  }

  data = { ...data }
  const errors: { [key: string]: any } = {}
  const remainingKeys = new Set(Object.keys(data))

  for (let key of ["autocompletionId"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNumber(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

/// Walk a few steps to next question not yet asked (and its answer).
// Include answer when it exists.
async function walkQuiz(
  rootSurvey: Survey,
  session: Session,
  state?: State,
): Promise<Step> {
  const domain = "interets"

  const autocompletion = Autocompletion.fromEntry(
    await db.oneOrNone(
      `
        SELECT *
        FROM autocompletions
        WHERE
          domain = $<domain>
          AND domain || '[' || id || ']' NOT IN (
            SELECT question_json->>'path'
            FROM asked_questions
            WHERE
              survey_segment = $<surveySegment>
              AND user_id = $<userId>
              AND question_json->>'path' LIKE $<domain> || '[%]'
          )
        ORDER BY id
        LIMIT 1
      `,
      {
        domain,
        surveySegment: rootSurvey.segment,
        userId: session.userId,
      },
    ),
  )
  return await newStep(autocompletion, domain, state, session)
}

/// Walk a single state to next question (and its answer).
async function walkList(
  rootSurvey: Survey,
  session: Session,
  state?: State,
): Promise<Step> {
  const autocompletionId: number | null =
    state === undefined ? null : state.autocompletionId
  const domain = "interets"

  const whereClauses = ["domain = $<domain>"]
  if (autocompletionId !== null) {
    whereClauses.push("id > $<autocompletionId>")
  }
  const autocompletion = Autocompletion.fromEntry(
    await db.oneOrNone(
      `
        SELECT *
        FROM autocompletions
        WHERE
          ${whereClauses.join(" AND ")}
          AND id NOT IN (
            SELECT substring(question_json->>'path' from '\\d+')::int
            FROM asked_questions
            WHERE
              survey_segment = $<surveySegment>
              AND user_id = $<userId>
              AND question_json->>'path' LIKE $<domain> || '[%]'
          )
        ORDER BY id
        LIMIT 1
      `,
      {
        autocompletionId,
        domain,
        surveySegment: rootSurvey.segment,
        userId: session.userId,
      },
    ),
  )

  return await newStep(autocompletion, domain, state, session)
}

export default Survey.register(
  new Survey(
    "interets",
    "Centres d'intérêts",
    walkQuiz,
    walkList,
    replaceQuestionState,
  ),
)
