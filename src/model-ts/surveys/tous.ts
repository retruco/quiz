import { Session } from "../session"
import { Step } from "../step"
import { Survey } from "../survey"

interface TousState {
  remainingSurveysSegments: string[]
  surveySegment: string | null
  surveyStateBySegment: { [surveySegment: string]: any }
}

async function replaceQuestionState(
  session: Session,
  currentState: TousState,
  questionState: any,
): Promise<[Step, any]> {
  if (currentState.surveySegment === null) {
    return [new Step(null, questionState), "Survey is finished"]
  }
  const survey = Survey.bySegment[currentState.surveySegment]
  const currentSurveyState =
    currentState.surveyStateBySegment[currentState.surveySegment]
  if (survey.replaceQuestionState === undefined) {
    return [
      new Step(null, questionState),
      `Survey "${
        currentState.surveySegment
      }" has no method to replace question state`,
    ]
  }
  const [surveyStep, stateError] = await survey.replaceQuestionState(
    session,
    currentSurveyState,
    questionState,
  )
  if (stateError !== null) {
    return [new Step(null, questionState), stateError]
  }
  return [
    new Step(surveyStep.question, {
      ...currentState,
      surveyStateBySegment: {
        ...currentState.surveyStateBySegment,
        [survey.segment]: surveyStep.state,
      },
    }),
    null,
  ]
}

async function walkQuiz(
  rootSurvey: Survey,
  session: Session,
  state?: TousState,
): Promise<Step> {
  return await walkWithWalker("walkQuiz", rootSurvey, session, state)
}

async function walkList(
  rootSurvey: Survey,
  session: Session,
  state?: TousState,
): Promise<Step> {
  return await walkWithWalker("walkList", rootSurvey, session, state)
}

async function walkWithWalker(
  walkerName: "walkQuiz" | "walkList",
  rootSurvey: Survey,
  session: Session,
  state?: TousState,
): Promise<Step> {
  const { remainingSurveysSegments, surveyStateBySegment } =
    state === undefined
      ? {
          remainingSurveysSegments: ["etat_civil", "interets"],
          surveyStateBySegment: {},
        }
      : state

  while (remainingSurveysSegments.length > 0) {
    const surveyIndex = Math.floor(
      Math.random() * remainingSurveysSegments.length,
    )
    const survey = Survey.bySegment[remainingSurveysSegments[surveyIndex]]
    const walker = survey[walkerName]
    if (walker === undefined) {
      // Survey is not active, remove it.
      remainingSurveysSegments.splice(surveyIndex, 1)
      continue
    }
    const surveyStep = await walker(
      rootSurvey,
      session,
      surveyStateBySegment[survey.segment],
    )
    surveyStateBySegment[survey.segment] = surveyStep.state
    if (surveyStep.question === undefined) {
      // Survey is finished.
      remainingSurveysSegments.splice(surveyIndex, 1)
      continue
    }
    return new Step(
      surveyStep.question,
      {
        remainingSurveysSegments,
        surveySegment: survey.segment,
        surveyStateBySegment,
      },
      surveyStep.answered,
      surveyStep.answer,
    )
  }
  return new Step(null, {
    remainingSurveysSegments,
    surveySegment: null,
    surveyStateBySegment,
  })
}

export default Survey.register(
  new Survey("tous", "Tous", walkQuiz, walkList, replaceQuestionState),
)
