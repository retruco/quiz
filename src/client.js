import * as sapper from "@sapper/app"
import "./model/questions/plugins"

sapper.start({
  // eslint-disable-next-line no-undef
  target: document.querySelector("#sapper"),
})
