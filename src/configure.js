import assert from "assert"

import { db, versionNumber } from "./model/database"

async function configureDatabase() {
  // Check that database exists.
  await db.connect()

  // Table: version
  await db.none(
    `
      CREATE TABLE IF NOT EXISTS version(
        number integer NOT NULL
      )
    `,
  )
  let version = await db.oneOrNone("SELECT * FROM version")
  if (version === null) {
    await db.none("INSERT INTO version(number) VALUES ($<number>)", {
      number: versionNumber,
    })
    version = await db.one("SELECT * FROM version")
  }
  assert(
    version.number <= versionNumber,
    `Database is too recent for current version of application: ${
      version.number
    } > ${versionNumber}.`,
  )
  if (version.number < versionNumber) {
    console.log(
      `Upgrading database from version ${
        version.number
      } to ${versionNumber}...`,
    )
  }

  // Table: clients
  await db.none(
    `
      CREATE TABLE IF NOT EXISTS clients (
        segment text NOT NULL PRIMARY KEY,
        name text NOT NULL,
        token text NOT NULL
      )
    `,
  )
  for (const command of [
    "COMMENT ON TABLE clients IS 'clients that can embed the quiz and execute surveys'",
    "COMMENT ON COLUMN clients.segment IS 'simplified name of client, as it appears" +
      " in URLs'",
    "COMMENT ON COLUMN clients.name IS 'user friendly name of the client'",
    "COMMENT ON COLUMN clients.token IS 'secret token use to authentify requests" +
      " coming from client'",
  ]) {
    await db.none(command)
  }

  // Table: users
  await db.none(
    `
      CREATE TABLE IF NOT EXISTS users (
        id bigserial NOT NULL PRIMARY KEY,
        client_segment text NOT NULL REFERENCES clients(segment),
        username text NOT NULL
      )
    `,
  )
  await db.none(
    `
      CREATE UNIQUE INDEX IF NOT EXISTS users_client_segment_username_idx
      ON users (client_segment, username)
    `,
  )
  for (const command of [
    "COMMENT ON TABLE users IS 'users that answers to questions'",
    "COMMENT ON COLUMN users.id IS 'unique ID of user'",
    "COMMENT ON COLUMN users.client_segment IS 'segment of the client'",
    "COMMENT ON COLUMN users.username IS 'username (unique only for the client)'",
  ]) {
    await db.none(command)
  }

  // Table: asked_questions
  await db.none(
    `
      CREATE TABLE IF NOT EXISTS asked_questions (
        id bigserial NOT NULL PRIMARY KEY,
        answered boolean NOT NULL DEFAULT false,
        client_segment text NOT NULL REFERENCES clients(segment),
        question_json jsonb NOT NULL,
        state jsonb,
        survey_segment text NOT NULL,
        user_id bigint NOT NULL REFERENCES users(id)
      )
    `,
  )
  for (const command of [
    "COMMENT ON TABLE asked_questions IS 'questions asked to users for clients'",
    "COMMENT ON COLUMN asked_questions.id IS 'unique ID of asked question'",
    "COMMENT ON COLUMN asked_questions.answered IS 'when true, the question has been" +
      " answered or skipped by user'",
    "COMMENT ON COLUMN asked_questions.client_segment IS 'segment of the client'",
    "COMMENT ON COLUMN asked_questions.question_json IS 'JSON of question'",
    "COMMENT ON COLUMN asked_questions.state IS 'state of survey that asked" +
      " question to user'",
    "COMMENT ON COLUMN asked_questions.survey_segment IS 'segment of survey that asked" +
      " question to user'",
    "COMMENT ON COLUMN asked_questions.user_id IS 'unique ID of the user to whom" +
      " question has been asked'",
  ]) {
    await db.none(command)
  }

  // Table: answers
  await db.none(
    `
      CREATE TABLE IF NOT EXISTS answers (
        external boolean NOT NULL DEFAULT false,
        question_path text NOT NULL,
        updated timestamp NOT NULL,
        user_id bigint NOT NULL REFERENCES users(id),
        value jsonb,
        PRIMARY KEY (user_id, question_path)
      )
    `,
  )
  for (const command of [
    "COMMENT ON TABLE answers IS 'answers of users to questions'",
    "COMMENT ON COLUMN answers.external IS 'when true, the value comes from an" +
      " external source (official…) and the question must not be asked'",
    "COMMENT ON COLUMN answers.question_path IS 'path of asked question'",
    "COMMENT ON COLUMN answers.updated IS 'date and time of last modification'",
    "COMMENT ON COLUMN answers.user_id IS 'unique ID of the user that answered" +
      " the question'",
    "COMMENT ON COLUMN answers.value IS 'JSON value of the answer'",
  ]) {
    await db.none(command)
  }

  // Table: sessions
  await db.none(
    `
      CREATE TABLE IF NOT EXISTS sessions (
        token text NOT NULL PRIMARY KEY,
        client_segment text NOT NULL REFERENCES clients(segment),
        expires timestamp NOT NULL,
        language text NOT NULL,
        user_id bigint NOT NULL REFERENCES users(id)
      )
    `,
  )
  await db.none(
    `
      CREATE UNIQUE INDEX IF NOT EXISTS
        sessions_client_segment_user_id_idx
      ON sessions (client_segment, user_id)
    `,
  )
  for (const command of [
    "COMMENT ON TABLE sessions IS 'temporary sessions of users using Quiz from clients'",
    "COMMENT ON COLUMN sessions.token IS 'randomly generated text that identifies" +
      " session'",
    "COMMENT ON COLUMN sessions.client_segment IS 'segment of the client" +
      " that the user is currently executing in'",
    "COMMENT ON COLUMN sessions.expires IS 'date and time the session will expire'",
    "COMMENT ON COLUMN sessions.language IS 'language code requested by user for" +
      " this survey'",
    "COMMENT ON COLUMN sessions.user_id IS 'unique ID of the user that is currently" +
      " executing in the client'",
  ]) {
    await db.none(command)
  }

  //
  // Plugins
  //

  // Table: autocompletions
  await db.none(
    `
      CREATE TABLE IF NOT EXISTS autocompletions (
        id bigserial NOT NULL PRIMARY KEY,
        domain text NOT NULL,
        text text NOT NULL,
        slug text NOT NULL,
        UNIQUE (domain, text)
      )
    `,
  )
  await db.none(
    `
      CREATE INDEX IF NOT EXISTS autocompletions_domain_idx
      ON autocompletions (domain)
    `,
  )
  await db.none(
    `
      CREATE INDEX IF NOT EXISTS autocompletions_domain_slug_idx
      ON autocompletions (domain, slug)
    `,
  )
  await db.none(
    `
      CREATE INDEX IF NOT EXISTS autocompletions_trigrams_idx
      ON autocompletions
      USING GIST (slug gist_trgm_ops)
    `,
  )
  for (const command of [
    "COMMENT ON TABLE autocompletions IS 'texts for autocomplete widget'",
    "COMMENT ON COLUMN autocompletions.id IS 'unique ID of the autocompletion text'",
    "COMMENT ON COLUMN autocompletions.domain IS 'allows to restrict autocompletion" +
      " on a subset of the texts'",
    "COMMENT ON COLUMN autocompletions.text IS 'an autocompleted text'",
    "COMMENT ON COLUMN autocompletions.slug IS 'simplified (slugified) version of text'",
  ]) {
    await db.none(command)
  }

  const previousVersionNumber = version.number

  version.number = versionNumber
  assert(
    version.number >= previousVersionNumber,
    `Error in database upgrade script: Wrong version number: ${
      version.number
    } < ${previousVersionNumber}.`,
  )
  if (version.number !== previousVersionNumber) {
    await db.none("UPDATE version SET number = $1", version.number)
    console.log(
      `Upgraded database from version ${previousVersionNumber} to ${
        version.number
      }.`,
    )
  }
}

configureDatabase()
  .then(() => process.exit(0))
  .catch(error => {
    console.log(error)
    process.exit(1)
  })
