import { AutocompletionWithDistance } from "../../../model/autocompletion"
import { db } from "../../../model/database"
import { Session } from "../../../model/session"
import { slugify } from "../../../model/strings"
import {
  validateMaybeTrimmedString,
  validateNonEmptyTrimmedString,
} from "../../../model/validators/core"

export async function get(req, res) {
  const [query, error] = validateQuery(req.query)
  if (error !== null) {
    console.error(
      `Error in ${req.path}:\n${JSON.stringify(
        query,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...query,
          error: {
            code: 400,
            details: error,
            message: "Invalid query",
          },
        },
        null,
        2,
      ),
    )
  }

  const autocompletions = (await db.any(
    `
      SELECT DISTINCT
        slug <-> $<term> AS distance,
        id,
        domain,
        text,
        slug
      FROM autocompletions
      WHERE domain = $<domain>
      ORDER BY distance ASC
      LIMIT $<limit>
    `,
    {
      domain: query.domain,
      limit: 10,
      term: slugify(query.q || ""),
    },
  )).map(AutocompletionWithDistance.fromEntry)

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(
    JSON.stringify(
      {
        autocompletions: autocompletions.map(autocompletion =>
          autocompletion.toJson(),
        ),
      },
      null,
      2,
    ),
  )
}

export async function post(req, res) {
  const [body, error] = await validateBody(req.body)
  if (error !== null) {
    console.error(
      `Error in ${req.path}:\n${JSON.stringify(
        body,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...body,
          error: {
            code: 400,
            details: error,
            message: "Invalid body",
          },
        },
        null,
        2,
      ),
    )
  }

  const session = Session.fromEntry(
    await db.oneOrNone(
      `
      SELECT *
      FROM sessions
      WHERE
        token = $<sessionToken>
    `,
      body,
    ),
  )
  if (session === null) {
    res.writeHead(401, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Invalid or obsolete session token",
          },
        },
        null,
        2,
      ),
    )
  }

  const { domain, slug, text } = body
  const entry = await db.oneOrNone(
    `
      INSERT INTO autocompletions (
        domain,
        text,
        slug
      )
      VALUES (
        $<domain>,
        $<text>,
        $<slug>
      )
      ON CONFLICT
      DO NOTHING
      RETURNING id
    `,
    {
      domain,
      slug,
      text,
    },
  )
  const id = entry === null ? null : Number(entry.id)

  const autocompletion = {
    domain,
    id,
    slug,
    text,
  }
  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(JSON.stringify(autocompletion, null, 2))
}

async function validateBody(data) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object got "${typeof data}"`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors = {}

  for (let key of ["domain", "sessionToken", "text"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  if (!errors.text) {
    const slug = slugify(data.text)
    if (!slug) {
      errors.text = "Text doesn't contain any meaningful character"
    } else {
      data.slug = slug
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

function validateQuery(query) {
  if (query === null || query === undefined) {
    return [query, "Missing query"]
  }
  if (typeof query !== "object") {
    return [query, `Expected an object, got ${typeof query}`]
  }

  query = {
    ...query,
  }
  const remainingKeys = new Set(Object.keys(query))
  const errors = {}

  {
    const key = "domain"
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(query[key])
    query[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "q"
    remainingKeys.delete(key)
    const [value, error] = validateMaybeTrimmedString(query[key])
    query[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [query, Object.keys(errors).length === 0 ? null : errors]
}
