import deepEqual from "deep-equal"

import { Answer } from "../../../model/answer"
import { AskedQuestion } from "../../../model/asked_question"
import { db } from "../../../model/database"
import { getAnswer } from "../../../model/orm"
import { Session } from "../../../model/session"
import {
  validateNumber,
  validateNonEmptyTrimmedString,
} from "../../../model/validators/core"

export async function post(req, res) {
  const [body, error] = validateBody(req.body)
  if (error !== null) {
    console.error(
      `Error in ${req.path}:\n${JSON.stringify(
        body,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...body,
          error: {
            code: 400,
            details: error,
            message: "Invalid body",
          },
        },
        null,
        2,
      ),
    )
  }

  const session = Session.fromEntry(
    await db.oneOrNone(
      `
      SELECT *
      FROM sessions
      WHERE
        token = $<sessionToken>
    `,
      body,
    ),
  )
  if (session === null) {
    res.writeHead(401, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Invalid or obsolete session token",
          },
        },
        null,
        2,
      ),
    )
  }

  const askedQuestion = AskedQuestion.fromEntry(
    await db.oneOrNone(
      `
        SELECT *
        FROM asked_questions
        WHERE
          id = $<askedQuestionId>
          AND survey_segment = $<surveySegment>
          AND user_id = $<userId>
      `,
      {
        askedQuestionId: body.askedQuestionId,
        surveySegment: body.surveySegment,
        userId: session.userId,
      },
    ),
  )
  if (askedQuestion === null) {
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 400,
            message:
              "Invalid asked question ID => Trying to answer to an unasked question",
          },
        },
        null,
        2,
      ),
    )
  }

  const [value, valueError] = askedQuestion.question.validateAnswerValueJson(
    body.value,
  )
  if (valueError !== null) {
    console.error(
      `Error in ${req.path}:\n${JSON.stringify(
        body,
        null,
        2,
      )}\n\nError:\n${JSON.stringify({ value: error }, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...body,
          error: {
            code: 400,
            details: { value: error },
            message: "Invalid body",
          },
          value,
        },
        null,
        2,
      ),
    )
  }


  let answer
  const previousAnswer = await getAnswer(askedQuestion.question.path, session.userId)
  if (previousAnswer === null) {
    answer = Answer.fromEntry(
      await db.one(
        `
          INSERT INTO answers (
            external,
            question_path,
            updated,
            user_id,
            value
          )
          VALUES (
            false,
            $<path>,
            current_timestamp,
            $<userId>,
            $<value:json>
          )
          RETURNING *
        `,
        {
          path: askedQuestion.question.path,
          userId: session.userId,
          value,
        },
      ),
    )

    // // Answer is added => Set all the following asked questions to unanswered.
    // await db.none(
    //   `
    //     UPDATE asked_questions
    //     SET
    //       answered = false
    //     WHERE
    //       id > $<askedQuestionId>
    //       AND survey_segment = $<surveySegment>
    //       AND user_id = $<userId>
    //   `,
    //   {
    //     askedQuestionId: body.askedQuestionId,
    //     surveySegment: body.surveySegment,
    //     userId: session.userId,
    //   },
    // )
    // Answer is added => Delete the following asked questions.
    await db.none(
      `
        DELETE FROM asked_questions
        WHERE
          id > $<askedQuestionId>
          AND survey_segment = $<surveySegment>
          AND user_id = $<userId>
      `,
      {
        askedQuestionId: body.askedQuestionId,
        surveySegment: body.surveySegment,
        userId: session.userId,
      },
    )
  } else if (deepEqual(previousAnswer.value, value)) {
    answer = Answer.fromEntry(
      await db.one(
        `
          UPDATE answers
          SET
            external = false,
            updated = current_timestamp
          WHERE
            question_path = $<path>
            AND user_id = $<userId>
          RETURNING *
        `,
        {
          path: askedQuestion.question.path,
          userId: session.userId,
        },
      ),
    )
  } else {
    answer = Answer.fromEntry(
      await db.one(
        `
          UPDATE answers
          SET
            external = false,
            updated = current_timestamp,
            value = $<value:json>
          WHERE
            question_path = $<path>
            AND user_id = $<userId>
          RETURNING *
        `,
        {
          path: askedQuestion.question.path,
          userId: session.userId,
          value,
        },
      ),
    )

    // // Answer is changed => Set all the following asked questions to unanswered.
    // await db.none(
    //   `
    //     UPDATE asked_questions
    //     SET
    //       answered = false
    //     WHERE
    //       id > $<askedQuestionId>
    //       AND survey_segment = $<surveySegment>
    //       AND user_id = $<userId>
    //   `,
    //   {
    //     askedQuestionId: body.askedQuestionId,
    //     surveySegment: body.surveySegment,
    //     userId: session.userId,
    //   },
    // )
    // Answer is changed => Delete the following asked questions.
    await db.none(
      `
        DELETE FROM asked_questions
        WHERE
          id > $<askedQuestionId>
          AND survey_segment = $<surveySegment>
          AND user_id = $<userId>
      `,
      {
        askedQuestionId: body.askedQuestionId,
        surveySegment: body.surveySegment,
        userId: session.userId,
      },
    )
  }

  // Mark asked question as answered.
  await db.none(
    `
      UPDATE asked_questions
      SET
        answered = true
      WHERE
        id = $<askedQuestionId>
        AND survey_segment = $<surveySegment>
        AND user_id = $<userId>
    `,
    {
      askedQuestionId: body.askedQuestionId,
      surveySegment: body.surveySegment,
      userId: session.userId,
    },
  )

  await db.none(
    `
      UPDATE sessions
      SET
        expires = current_timestamp + interval '1 day'
      WHERE
        token = $<token>
    `,
    {
      token: session.token,
    },
  )

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(JSON.stringify(answer.toJson(), null, 2))
}

function validateBody(data) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object, got ${typeof data}`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors = {}

  {
    const key = "askedQuestionId"
    remainingKeys.delete(key)
    const [value, error] = validateNumber(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (const key of ["sessionToken", "surveySegment"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "value"
    remainingKeys.delete(key)
    const [value, error] = [data[key], null] // Validation is done later.
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}
