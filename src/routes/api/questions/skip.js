import { db } from "../../../model/database"
import { AskedQuestion } from "../../../model/asked_question"
import { Session } from "../../../model/session"
import {
  validateNumber,
  validateNonEmptyTrimmedString,
} from "../../../model/validators/core"

export async function post(req, res) {
  const [body, error] = validateBody(req.body)
  if (error !== null) {
    console.error(
      `Error in ${req.path}:\n${JSON.stringify(
        body,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...body,
          error: {
            code: 400,
            details: error,
            message: "Invalid body",
          },
        },
        null,
        2,
      ),
    )
  }

  const session = Session.fromEntry(
    await db.oneOrNone(
      `
      SELECT *
      FROM sessions
      WHERE
        token = $<sessionToken>
    `,
      body,
    ),
  )
  if (session === null) {
    res.writeHead(401, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Invalid or obsolete session token",
          },
        },
        null,
        2,
      ),
    )
  }

  const askedQuestion = AskedQuestion.fromEntry(
    await db.oneOrNone(
      `
        SELECT *
        FROM asked_questions
        WHERE
          id = $<askedQuestionId>
          AND survey_segment = $<surveySegment>
          AND user_id = $<userId>
      `,
      {
        askedQuestionId: body.askedQuestionId,
        surveySegment: body.surveySegment,
        userId: session.userId,
      },
    ),
  )
  if (askedQuestion === null) {
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 400,
            message:
              "Invalid asked question ID => Trying to answer to an unasked question",
          },
        },
        null,
        2,
      ),
    )
  }

  // Mark asked question as answered (skipped).
  await db.none(
    `
      UPDATE asked_questions
      SET
        answered = true
      WHERE
        id = $<askedQuestionId>
        AND survey_segment = $<surveySegment>
        AND user_id = $<userId>
    `,
    {
      askedQuestionId: body.askedQuestionId,
      surveySegment: body.surveySegment,
      userId: session.userId,
    },
  )

  await db.none(
    `
      UPDATE sessions
      SET
        expires = current_timestamp + interval '1 day'
      WHERE
        token = $<token>
    `,
    {
      token: session.token,
    },
  )

  res.writeHead(204)
  res.end()
}

function validateBody(data) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object, got ${typeof data}`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors = {}

  {
    const key = "askedQuestionId"
    remainingKeys.delete(key)
    const [value, error] = validateNumber(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (const key of ["sessionToken", "surveySegment"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}
