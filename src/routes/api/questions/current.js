import deepCopy from "deepcopy"

import { AskedQuestion } from "../../../model/asked_question"
import { db } from "../../../model/database"
import { getAnswer } from "../../../model/orm"
import { Session } from "../../../model/session"
import { Survey } from "../../../model/survey"
import { validateNonEmptyTrimmedString } from "../../../model/validators/core"

export async function post(req, res) {
  const [body, error] = validateBody(req.body)
  if (error !== null) {
    console.error(
      `Error in ${req.path}:\n${JSON.stringify(
        body,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...body,
          error: {
            code: 400,
            details: error,
            message: "Invalid body",
          },
        },
        null,
        2,
      ),
    )
  }

  const session = Session.fromEntry(
    await db.oneOrNone(
      `
        SELECT *
        FROM sessions
        WHERE
          token = $<sessionToken>
      `,
      body,
    ),
  )
  if (session === null) {
    res.writeHead(401, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Invalid or obsolete session token",
          },
        },
        null,
        2,
      ),
    )
  }

  const survey = Survey.bySegment[body.surveySegment]
  if (survey === undefined) {
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 400,
            message: "Invalid survey segment in session",
          },
        },
        null,
        2,
      ),
    )
  }

  let current
  let askedQuestion = AskedQuestion.fromEntry(
    await db.oneOrNone(
      `
        SELECT *
        FROM asked_questions
        WHERE
          survey_segment = $<surveySegment>
          AND user_id = $<userId>
          AND answered = false
        ORDER BY id
        LIMIT 1
      `,
      {
        surveySegment: body.surveySegment,
        userId: session.userId,
      },
    ),
  )
  if (askedQuestion !== null) {
    const answer = await getAnswer(askedQuestion.question.path, session.userId)
    current = {
      answer: answer === null ? null : answer.toJson(),
      askedQuestionId: askedQuestion.id,
      language: session.language,
      question: askedQuestion.question.toJson(),
      survey: survey.toJson(),
    }
  } else {
    // Every existing asked question is answered. Retrieve the last answered
    // one and walk to the next (unanswered) one.
    const lastAnsweredAskedQuestion = AskedQuestion.fromEntry(
      await db.oneOrNone(
        `
          SELECT *
          FROM asked_questions
          WHERE
            survey_segment = $<surveySegment>
            AND user_id = $<userId>
            AND answered = true
          ORDER BY id DESC
          LIMIT 1
        `,
        {
          surveySegment: body.surveySegment,
          userId: session.userId,
        },
      ),
    )
    let previousState =
      lastAnsweredAskedQuestion === null || lastAnsweredAskedQuestion.state === null
        ? undefined
        : lastAnsweredAskedQuestion.state

    for (;;) {
      const { answer, answered, question, state } = await survey.walkQuiz(
        survey,
        session,
        deepCopy(previousState),
      )
      if (question === undefined) {
        // Survey is finished.
        current = {
          answer: null,
          askedQuestionId: null,
          language: session.language,
          question: null,
          survey: survey.toJson(),
        }
        break
      }
      askedQuestion = AskedQuestion.fromEntry(
        await db.one(
          `
            INSERT INTO asked_questions (
              answered,
              client_segment,
              question_json,
              state,
              survey_segment,
              user_id
            )
            VALUES (
              $<answered>,
              $<clientSegment>,
              $<questionJson:json>,
              $<state:json>,
              $<surveySegment>,
              $<userId>
            )
            RETURNING *
          `,
          {
            answered,
            clientSegment: session.clientSegment,
            questionJson: question.toJson(),
            state,
            surveySegment: body.surveySegment,
            userId: session.userId,
          },
        ),
      )
      if (!answered) {
        current = {
          answer: answer === null || answer === undefined ? null : answer.toJson(),
          askedQuestionId: askedQuestion.id,
          language: session.language,
          question: question.toJson(),
          survey: survey.toJson(),
        }
        break
      }
      previousState = state
    }
  }

  await db.none(
    `
      UPDATE sessions
      SET
        expires = current_timestamp + interval '1 day'
      WHERE
        token = $<token>
    `,
    {
      askedQuestionId: askedQuestion === null ? null : askedQuestion.id,
      token: session.token,
    },
  )

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(JSON.stringify(current, null, 2))
}

function validateBody(data) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object, got ${typeof data}`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors = {}

  for (const key of ["sessionToken", "surveySegment"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}
