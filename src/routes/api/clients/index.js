import { Client } from "../../../model/client"
import { db } from "../../../model/database"

export async function get(req, res) {
  const clientsJson = (await db.any(
    `
      SELECT *
      FROM clients
      ORDER BY name
    `,
  ))
    .map(Client.fromEntry)
    .map(client => client.toJson())

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(
    JSON.stringify(
      {
        clients: clientsJson,
      },
      null,
      2,
    ),
  )
}
