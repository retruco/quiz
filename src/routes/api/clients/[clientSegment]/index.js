import { Client } from "../../../../model/client"
import { db } from "../../../../model/database"

export async function get(req, res) {
  const { clientSegment } = req.params

  const client = Client.fromEntry(
    await db.oneOrNone(
      `
        SELECT *
        FROM clients
        where segment = $<clientSegment>
      `,
      {
        clientSegment,
      },
    ),
  )
  if (client === null) {
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 400,
            message: "Unknown client",
          },
        },
        null,
        2,
      ),
    )
  }

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(JSON.stringify(client.toJson(), null, 2))
}
