import { randomBytes } from "crypto"

import { Client } from "../../../../model/client"
import { db } from "../../../../model/database"
import { acceptedLanguages } from "../../../../locales"
import { Session } from "../../../../model/session"
import { Survey } from "../../../../model/survey"
import { User } from "../../../../model/user"
import {
  validateChoice,
  validateNonEmptyTrimmedString,
} from "../../../../model/validators/core"

async function generateToken() {
  return new Promise(resolve =>
    randomBytes(48, (err, buffer) => resolve(buffer.toString("hex"))),
  )
}

export async function post(req, res) {
  const { clientSegment } = req.params

  const [body, error] = validateBody(req.body)
  if (error !== null) {
    console.error(
      `Error in ${req.path}:\n${JSON.stringify(
        body,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...body,
          error: {
            code: 400,
            details: error,
            message: "Invalid body",
          },
        },
        null,
        2,
      ),
    )
  }

  const client = Client.fromEntry(
    await db.oneOrNone(
      `
        SELECT *
        FROM clients
        where segment = $<clientSegment>
      `,
      {
        clientSegment,
      },
    ),
  )
  if (client === null) {
    res.writeHead(401, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Unknown client",
          },
        },
        null,
        2,
      ),
    )
  }

  if (client.token !== body.clientToken) {
    res.writeHead(401, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Invalid client token",
          },
        },
        null,
        2,
      ),
    )
  }

  let user = User.fromEntry(
    await db.oneOrNone(
      `
        SELECT *
        FROM users
        WHERE
          client_segment = $<clientSegment>
          AND username = $<username>
      `,
      {
        clientSegment,
        username: body.username,
      },
    ),
  )
  if (user === null) {
    user = User.fromEntry(
      await db.one(
        `
          INSERT INTO users (
            client_segment,
            username
          )
          VALUES (
            $<clientSegment>,
            $<username>
          )
          RETURNING *
        `,
        {
          clientSegment,
          username: body.username,
        },
      ),
    )
  }

  let session = Session.fromEntry(
    await db.oneOrNone(
      `
        SELECT *
        FROM sessions
        WHERE
          client_segment = $<clientSegment>
          AND user_id = $<userId>
      `,
      {
        clientSegment,
        userId: user.id,
      },
    ),
  )
  if (session === null) {
    session = Session.fromEntry(
      await db.one(
        `
          INSERT INTO sessions (
            token,
            client_segment,
            expires,
            language,
            user_id
          )
          VALUES (
            $<token>,
            $<clientSegment>,
            current_timestamp + interval '1 day',
            $<language>,
            $<userId>
          )
          RETURNING *
        `,
        {
          clientSegment,
          language: body.language,
          token: await generateToken(),
          userId: user.id,
        },
      ),
    )
  } else {
    session = Session.fromEntry(
      await db.one(
        `
        UPDATE sessions
        SET
          expires = current_timestamp + interval '1 day',
          language = $<language>
        WHERE
          client_segment = $<clientSegment>
          AND user_id = $<userId>
        RETURNING *
      `,
        {
          clientSegment,
          language: body.language,
          userId: user.id,
        },
      ),
    )
  }

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(
    JSON.stringify(
      { sessionToken: session.token, surveySegment: body.surveySegment },
      null,
      2,
    ),
  )
}

function validateBody(data) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object, got ${typeof data}`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors = {}

  for (const key of ["clientToken", "username"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "language"
    remainingKeys.delete(key)
    const [value, error] = validateChoice(acceptedLanguages)(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "surveySegment"
    remainingKeys.delete(key)
    const [value, error] = validateChoice(Object.keys(Survey.bySegment))(
      data[key],
    )
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}
