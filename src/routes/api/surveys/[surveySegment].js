import deepCopy from "deepcopy"

import { AskedQuestion } from "../../../model/asked_question"
import { db } from "../../../model/database"
import { getAnswer } from "../../../model/orm"
import { Session } from "../../../model/session"
import { Survey } from "../../../model/survey"
import { validateNonEmptyTrimmedString } from "../../../model/validators/core"

export async function get(req, res) {
  const { surveySegment } = req.params
  const survey = Survey.bySegment[surveySegment]
  if (survey === undefined) {
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 400,
            message: "Unknown survey",
          },
        },
        null,
        2,
      ),
    )
  }

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(JSON.stringify(survey.toJson(), null, 2))
}

export async function post(req, res) {
  const { surveySegment } = req.params
  const survey = Survey.bySegment[surveySegment]
  if (survey === undefined) {
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 400,
            message: "Unknown survey",
          },
        },
        null,
        2,
      ),
    )
  }

  const [body, error] = validateBody(req.body)
  if (error !== null) {
    console.error(
      `Error in ${req.path}:\n${JSON.stringify(
        body,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...body,
          error: {
            code: 400,
            details: error,
            message: "Invalid body",
          },
        },
        null,
        2,
      ),
    )
  }

  const session = Session.fromEntry(
    await db.oneOrNone(
      `
      SELECT *
      FROM sessions
      WHERE
        token = $<sessionToken>
    `,
      body,
    ),
  )
  if (session === null) {
    res.writeHead(401, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Invalid or obsolete session token",
          },
        },
        null,
        2,
      ),
    )
  }

  const items = []
  const askedQuestions = (await db.any(
    `
      SELECT *
      FROM asked_questions
      WHERE
        survey_segment = $<surveySegment>
        AND user_id = $<userId>
      ORDER BY id
    `,
    {
      surveySegment,
      userId: session.userId,
    },
  )).map(AskedQuestion.fromEntry)
  for (const askedQuestion of askedQuestions) {
    const answer = await getAnswer(askedQuestion.question.path, session.userId)
    items.push({
      answer: answer === null ? null : answer.toJson(),
      askedQuestionId: askedQuestion.id,
      question: askedQuestion.question.toJson(),
    })
  }

  const lastAskedQuestion = askedQuestions[askedQuestions.length - 1]
  let previousState =
    lastAskedQuestion === undefined ? undefined : lastAskedQuestion.state
  for (;;) {
    const { answer, question, state } = await survey.walkList(
      survey,
      session,
      deepCopy(previousState),
    )
    if (question === undefined) {
      break
    }
    items.push({
      answer: answer === undefined ? null : answer.toJson(),
      question: question === undefined ? null : question.toJson(),
    })
    previousState = state
  }

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(
    JSON.stringify(
      {
        items,
        name: survey.name,
        segment: surveySegment,
      },
      null,
      2,
    ),
  )
}

function validateBody(data) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object, got ${typeof data}`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors = {}

  {
    const key = "sessionToken"
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}
