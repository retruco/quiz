import { Session } from "../../../model/session"
import { Survey } from "../../../model/survey"

import { db } from "../../../model/database"
import { validateNonEmptyTrimmedString } from "../../../model/validators/core"

export async function get(req, res) {
  const surveys = Object.values(Survey.bySegment)
    .sort((a, b) => a.name.localeCompare(b.name))
    .map(survey => survey.toJson())

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(
    JSON.stringify(
      {
        surveys,
      },
      null,
      2,
    ),
  )
}

/// Return the surveys run directly by clients for corrent user.
export async function post(req, res) {
  const [body, error] = validateBody(req.body)
  if (error !== null) {
    console.error(
      `Error in ${req.path}:\n${JSON.stringify(
        body,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...body,
          error: {
            code: 400,
            details: error,
            message: "Invalid body",
          },
        },
        null,
        2,
      ),
    )
  }

  const session = Session.fromEntry(
    await db.oneOrNone(
      `
      SELECT *
      FROM sessions
      WHERE
        token = $<sessionToken>
    `,
      body,
    ),
  )
  if (session === null) {
    res.writeHead(401, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Invalid or obsolete session token",
          },
        },
        null,
        2,
      ),
    )
  }

  const userMainSurveysSegments = (await db.any(
    `
      SELECT DISTINCT survey_segment
      FROM asked_questions
      WHERE user_id = $<userId>
    `,
    {
      userId: session.userId,
    },
  )).map(entry => entry.survey_segment)

  const userMainSurveysJson = userMainSurveysSegments
    .map(surveySegment => Survey.bySegment[surveySegment])
    .filter(survey => survey !== undefined)
    .sort((a, b) => a.name.localeCompare(b.name))
    .map(survey => survey.toJson())

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(
    JSON.stringify(
      {
        surveys: userMainSurveysJson,
      },
      null,
      2,
    ),
  )
}

function validateBody(data) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object, got ${typeof data}`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors = {}

  {
    const key = "sessionToken"
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}
