import { CachedSyncIterable } from "cached-iterable"
import { FluentBundle } from "fluent"
import { mapBundleSync } from "fluent-sequence"
import { negotiateLanguages } from "fluent-langneg"

export const acceptedLanguages = ["en", "en-US", "fr", "fr-FR"]
const localizedLanguages = ["en-US", "fr-FR"]
export const proposedLanguagesAndLabels = [
  ["en", "English"],
  ["fr", "Français"],
]

const bundleByLanguage = {}
const messagesByLanguage = {
  "en-US": `
About = About
About_site = About this site
Ask_questions_forever = Ask questions again and again…
Clients = Clients
Create = Create
Home = Home
No = No
Retruco_Quiz = Retruco Quiz
See_all_knowledge = See all knowledge
See_survey_on_one_page = See survey on one page
Select = Select
Surveys = Surveys
User_Surveys = Main Surveys Filled by User
Yes = Yes
  `,
  "fr-FR": `
About = À propos
About_site = À propos de ce site
Ask_questions_forever = Pose des questions en permanence…
Clients = Clients
Create = Créer
Home = Accueil
No = Non
Retruco_Quiz = Retruco Quiz
See_all_knowledge = Voir toutes les connaissances
See_survey_on_one_page = Voir le questionnaire sur une seule page
Select = Choisir
Surveys = Questionnaires
User_Surveys = Questionnaires principaux remplis par l'utilisateur
Yes = Oui
  `,
}

/*
 * `Localization` handles translation formatting and fallback.
 *
 * The current negotiated fallback chain of languages is stored in the
 * `Localization` instance in form of an iterable of `FluentBundle`
 * instances.  This iterable is used to find the best existing translation for
 * a given identifier.
 *
 * Code taken from: https://github.com/projectfluent/fluent.js/blob/master/fluent-react/src/localization.js
 */
export class Localization {
  constructor(bundles) {
    this.bundles = CachedSyncIterable.from(bundles)
  }

  getBundle(id) {
    return mapBundleSync(this.bundles, id)
  }

  /// Find a translation by `id` and format it to a string using `args`.
  getString(id, args, fallback) {
    const bundle = this.getBundle(id)

    if (bundle === null) {
      return fallback || id
    }

    const msg = bundle.getMessage(id)
    return bundle.format(msg, args)
  }
}

export function* generateBundles(userLanguages) {
  // Choose locales that are best for the user.
  const currentLanguages = negotiateLanguages(
    userLanguages,
    localizedLanguages,
    {
      defaultLocale: "en",
    },
  )

  for (const language of currentLanguages) {
    yield bundleByLanguage[language]
  }
}

for (const language of localizedLanguages) {
  const bundle = new FluentBundle(language)
  const errors = bundle.addMessages(messagesByLanguage[language])
  if (errors.length > 0) {
    console.warn(
      `Errors occurred when adding messages for "${language}" language:\n${JSON.stringify(
        errors,
        null,
        2,
      )}`,
    )
  }
  bundleByLanguage[language] = bundle
}
